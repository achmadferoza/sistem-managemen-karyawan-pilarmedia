// Call the dataTables jQuery plugin
$(document).ready(function() {
  var table = $('#dataTable').DataTable({
    responsive: true
  });
  $('#dataTable_filter').css('display','none');
  $('#date').on('change', function () {
    table.search( this.value ).draw();
  });
});
