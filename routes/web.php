<?php

use Illuminate\Support\Facades\Route;

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/
Auth::routes();
Route::get('/daftar', [App\Http\Controllers\UserController::class, 'daftar']);
Route::post('/daftar/tambah', [App\Http\Controllers\UserController::class, 'daftar_tambah']);
Route::group(['middleware' => ['auth']], function () {
    //Route Untuk Admin
    Route::group(['middleware' => ['checkRole:1']], function () {
        Route::get('/dashboard', [App\Http\Controllers\DashboardController::class, 'index'])->name('dashboard.index');
        Route::get('/kehadiran/{id}', [App\Http\Controllers\DashboardController::class, 'kehadiran_user']);
        Route::delete('/absen/hapus/{id}', [App\Http\Controllers\AbsenController::class, 'hapus'])->name('absen.hapus');
        Route::get('/users', [App\Http\Controllers\UserController::class, 'index']);
        Route::post('/users/tambah', [App\Http\Controllers\UserController::class, 'tambah'])->name('user.tambah');
        Route::delete('/users/hapus/{id}', [App\Http\Controllers\UserController::class, 'hapus'])->name('user.hapus');
    });

    //Route Untuk Karyawan
    Route::group(['middleware' => ['checkRole:2']], function () {
        Route::get('/kehadiran', [App\Http\Controllers\DashboardController::class, 'kehadiran']);
        Route::post('/checkin', [App\Http\Controllers\AbsenController::class, 'checkin'])->name('absen.checkin');
        Route::post('/checkout', [App\Http\Controllers\AbsenController::class, 'checkout'])->name('absen.checkout');

    });

    //Route Untuk Admin dan Karyawan
    Route::get('/', [App\Http\Controllers\AbsenController::class, 'index'])->name('absen.index');
    Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');
    Route::get('/profile', [App\Http\Controllers\UserController::class, 'profile']);
    Route::patch('/profile', [App\Http\Controllers\UserController::class, 'update'])->name('user.update');
    Route::patch('/profile/ubah_password', [App\Http\Controllers\UserController::class, 'ubah_password'])->name('user.ubah_password');
    Route::get('/logout', '\App\Http\Controllers\Auth\LoginController@logout');

});
