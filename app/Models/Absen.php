<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Absen extends Model
{
    use HasFactory;

    protected $table = 'absen';
    protected $guarded = ['id'];
    public $timestamps = false;

    //Relasi pada tabel CheckIn
    public function checkin(){
        return $this->hasMany(Checkin::class, 'absen_id');
    }
    //Relasi pada tabel CheckOut
    public function checkout(){
        return $this->hasMany(Checkout::class, 'absen_id');
    }
    //Relasi pada tabel User
    public function user(){
        return $this->belongsTo(User::class, 'user_id');
    }

}
