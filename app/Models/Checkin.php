<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Checkin extends Model
{
    use HasFactory;
    protected $table = 'check_in';
    protected $guarded = ['id'];
    public $timestamps = false;

    //Relasi pada tabel Absen
    public function absen(){
        return $this->belongsTo(Absen::class, 'absen_id');
    }
}
