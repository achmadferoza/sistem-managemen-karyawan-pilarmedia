<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\User;
use Hash;

class UserController extends Controller
{
    //Fungsi Untuk Menampilkan Halaman Daftar User
    public function index(){
        $data= User::get();
        return view('dashboard.users', compact('data'));
    }

    //Fungsi Untuk Menampilkan Halaman Profile
    public function profile()
    {
        $data = User::find(auth()->user()->id);
        return view('dashboard.profile', compact('data'));
    }

    //Fungsi Untuk Menambahkan User
    public function tambah(Request $request)
    {
        $request->validate([
            'nama' => 'required|min:3|unique:users',
            'username' => 'required|min:5|unique:users',
            'password' => 'required|min:8',
            'role_id' => 'required',
        ]);
        $params = [
            'nama' => $request->nama,
            'username' => $request->username,
            'password' => Hash::make($request->password),
            'foto' => $request->foto,
            'phone_number' => $request->phone_number,
            'role_id' => $request->role_id,
        ];
        try {
            if ($params['foto'] !== null) {
                $path = $this->upload_gambar($params['foto']);
                $params['foto'] = $path;
            } else {
                $params['foto'] = '/storage/user/default.jpg';
            }
            $data = User::create($params);
            $data->save();
        } catch (\Throwable $e) {
            return $e;
            alert()->error('User Gagal Ditambah', 'User');
            return redirect()->back();
        }   
        alert()->success('User Berhasil ditambah', 'User');
        return redirect()->back();
    }

    //Fungsi Untuk Mengubah User
    public function update(Request $request)
    {
        $request->validate([
            'nama' => 'required|min:3',
            'username' => 'sometimes|min:5',
        ]);
        $params = [
            'nama' => $request->nama,
            'username' => $request->username,
            'foto' => $request->foto,
        ];
        try {
            $data = User::findOrFail(auth()->user()->id);
            $data->nama = $request->nama;
            $data->username = $request->username;
            $data->password = $data->password;
            $data->phone_number = $request->phone_number;
            if ($params['foto'] !== null) {
                $str = str_replace('/storage', '', $data->foto);
                if ($data->foto !== "/storage/user/default.jpg") {
                    unlink(storage_path('app/public' . $str));
                }
                $imagePath = $params['foto'];
                $jalur = time() . '-' . $imagePath->getClientOriginalName();
                $path = $params['foto']->storeAs('user', $jalur, 'public');
                $path = '/storage/user/' . $jalur;
                $data->foto = $path;
            } else {
                $data->foto = $data->foto;
            }
            $data->update();
        } catch (\Throwable $e) {
            alert()->error('User Gagal Diupdate', 'User');
            return redirect()->back();
        }
        alert()->success('User Berhasil diubah', 'User');
        return redirect()->back();

    }

    //Fungsi Untuk Mengubah Password
    public function ubah_password(Request $request)
    {
        $user = User::find(auth()->user()->id);
        $request->validate([
            'password'                => 'required|min:6',
            'password_baru'           => 'required|min:6|required_with:konfirmasi_password|same:konfirmasi_password',
            'konfirmasi_password'     => 'required|min:6'
        ]);

        if (Hash::check(  $request->password, $user->password)) {
            if ($request->password == $request->konfirmasi_password) {
                alert()->error('Password gagal diperbarui, tidak ada yang berubah pada kata sandi', 'Password');
                return redirect()->back();
            } else {
                $user->password = Hash::make($request->konfirmasi_password);
                $user->save();
                alert()->success('Password berhasil diperbarui', 'Password');
                return redirect()->back();
            }
        } else {
            alert()->error('Password tidak cocok dengan kata sandi lama', 'Password');
            return redirect()->back();
        }
    }

    //Fungsi Untuk Menghapus User
    public function hapus($id){
        $data = User::findOrFail($id);
        try {
            $data->delete();
        } catch (\Throwable $e) {
            alert()->error('User Gagal Dihapus', 'User');
            return redirect()->back();
        }
        alert()->success('User Berhasil Dihapus', 'User');
        return redirect()->back();
    }

    //Fungsi Untuk Menampilkan Halaman Daftar
    public function daftar()
    {
        return view('auth.daftar');
    }

    // Fungsi Untuk Daftar User
    public function daftar_tambah(Request $request){
        $request->validate([
            'nama' => 'required|min:3|unique:users',
            'username' => 'required|min:5|unique:users',
            'password' => 'required|min:8',
            'phone_number' => 'required',
        ]);
        $params = [
            'nama' => $request->nama,
            'username' => $request->username,
            'password' => Hash::make($request->password),
            'foto' => $request->foto,
            'phone_number' => '+62'.$request->phone_number,
            'role_id' => 2,
        ];
        try {
            if ($params['foto'] !== null) {
                $path = $this->upload_gambar($params['foto']);
                $params['foto'] = $path;
            } else {
                $params['foto'] = '/storage/user/default.jpg';
            }
            $data = User::create($params);
            $data->save();
        } catch (\Throwable $e) {
            alert()->error('Gagal Daftar', 'Daftar');
            return redirect()->back();
        }   
        alert()->success('Berhasil Daftar', 'Daftar');
        return redirect()->route('login');
    }
}
