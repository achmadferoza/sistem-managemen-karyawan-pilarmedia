<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use App\Models\Absen;
use App\Models\User;
use App\Models\Checkin;
use App\Models\Checkout;
use DateTime;
use DB;

class DashboardController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */

     //Fungsi Untuk Menampilkan Halaman Dashboard
    public function index()
    {
        //Untuk Menghitung Total Status Checkin
        $checkin = Absen::whereUserId(auth()->user()->id)->whereHas('checkin', function($q){
            $q->where('keterangan', '=', 'Masuk');
        })->get();
        $telat = Absen::whereUserId(auth()->user()->id)->whereHas('checkin', function($q){
            $q->where('keterangan', '=', 'Telat');
        })->get();
        $alpha = Absen::whereUserId(auth()->user()->id)->whereHas('checkin', function($q){
            $q->where('keterangan', '=', 'Alpha');
        })->get();

        $data = Absen::get();

        //Fungsi Untuk Menampilkan Grafik
        $chart = array();
        $bulan = array();
        $chart['status'] = ['Ontime','Telat', 'Alpha'];
        for ($i=0; $i < 3; $i++) { 
            $chart['warna'][$i] = '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);
        }
        $grafik = DB::table("absen")
            ->join('check_in', 'absen.id', '=', 'check_in.id')
            ->select(DB::raw('MONTH(absen.tanggal) as `bulan`'), 'tanggal', 'check_in.keterangan')
            ->get();
        if(!empty($grafik)){

            foreach($grafik as $key => $item)
            {
                $bulan[] = DateTime::createFromFormat('!m', $item->bulan)->format('F');
                if(!isset($chart['total'])){
                    $chart['total']['ontime']['bulan_'. $item->bulan] = 0;
                    $chart['total']['telat']['bulan_'. $item->bulan] = 0;
                    $chart['total']['alpha']['bulan_'. $item->bulan] = 0;
                }
                if($item->keterangan == 'Masuk'){
                    $chart['total']['ontime']['bulan_'. $item->bulan] += 1;
                }elseif($item->keterangan == 'Telat'){
                    $chart['total']['telat']['bulan_'. $item->bulan] += 1;
                }else{
                    $chart['total']['alpha']['bulan_'. $item->bulan] += 1;
                }
            }
            $chart['bulan'] = array_unique($bulan);
        }
        return view('dashboard.index', compact('checkin', 'alpha', 'telat', 'data',  'chart'));
    }

    //Fungsi Untuk Menampilkan Halaman Kehadiran
    public function kehadiran()
    {
        $checkin = Absen::whereUserId(auth()->user()->id)->whereHas('checkin', function($q){
            $q->where('keterangan', '=', 'Masuk');
        })->get();
        $telat = Absen::whereUserId(auth()->user()->id)->whereHas('checkin', function($q){
            $q->where('keterangan', '=', 'Telat');
        })->get();
        $alpha = Absen::whereUserId(auth()->user()->id)->whereHas('checkin', function($q){
            $q->where('keterangan', '=', 'Alpha');
        })->get();

        $data = Absen::whereUserId(auth()->user()->id)->get();
        return view('dashboard.kehadiran', compact('checkin', 'alpha', 'telat', 'data'));
    }

    //Fungsi Untuk Menampilkan Halaman Kehadiran (Hak Akses Admin)
    public function kehadiran_user($id){
         //Untuk Menghitung Total Status Checkin
        $checkin = Absen::whereUserId($id)->whereHas('checkin', function($q){
            $q->where('keterangan', '=', 'Masuk');
        })->get();
        $telat = Absen::whereUserId($id)->whereHas('checkin', function($q){
            $q->where('keterangan', '=', 'Telat');
        })->get();
        $alpha = Absen::whereUserId($id)->whereHas('checkin', function($q){
            $q->where('keterangan', '=', 'Alpha');
        })->get();

        $data = Absen::whereUserId($id)->get();
        $user = User::find($id);

        //Fungsi Untuk Menampilkan Grafik
        $chart = array();
        $bulan = array();
        $chart['status'] = ['Ontime','Telat', 'Alpha'];
        for ($i=0; $i < 3; $i++) { 
            $chart['warna'][$i] = '#' . substr(str_shuffle('ABCDEF0123456789'), 0, 6);
        }
        $grafik = DB::table("absen")
            ->join('check_in', 'absen.id', '=', 'check_in.id')
            ->select(DB::raw('MONTH(absen.tanggal) as `bulan`'), 'tanggal', 'check_in.keterangan')
            ->where('user_id' , '=' , $id)
            ->get();
        if(!empty($grafik)){

            foreach($grafik as $key => $item)
            {
                $bulan[] = DateTime::createFromFormat('!m', $item->bulan)->format('F');
                if(!isset($chart['total'])){
                    $chart['total']['ontime']['bulan_'. $item->bulan] = 0;
                    $chart['total']['telat']['bulan_'. $item->bulan] = 0;
                    $chart['total']['alpha']['bulan_'. $item->bulan] = 0;
                }
                if($item->keterangan == 'Masuk'){
                    $chart['total']['ontime']['bulan_'. $item->bulan] += 1;
                }elseif($item->keterangan == 'Telat'){
                    $chart['total']['telat']['bulan_'. $item->bulan] += 1;
                }else{
                    $chart['total']['alpha']['bulan_'. $item->bulan] += 1;
                }
            }
            $chart['bulan'] = array_unique($bulan);
        }
        return view('dashboard.kehadiran-user', compact('checkin', 'alpha', 'telat', 'data', 'user','chart'));
    }
}
