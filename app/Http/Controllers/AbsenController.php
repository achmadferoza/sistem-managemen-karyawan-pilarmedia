<?php

namespace App\Http\Controllers;

use App\Models\Absen;
use App\Models\User;
use App\Models\Checkin;
use App\Models\Checkout;
use App\Http\Resources\AbsenResource;
use Illuminate\Http\Request;

class AbsenController extends Controller
{

    // Fungsi Untuk Menampilkan Form Absen
    public function index()
    {
        $absen = Absen::whereUserId(auth()->user()->id)->whereTanggal(date('Y-m-d'))->first();
        $checkout = null;
        $alpha = false;
        // Untuk Melakukan Kondisi Pada Status Absen Tertentu
        if(!empty($absen)){
            $checkout = Checkout::whereAbsenId($absen->id)->get();
            $checkin = Checkin::whereAbsenId($absen->id)->first();
            if(!empty($checkin)){
                if($checkin->keterangan == 'Alpha'){
                    $alpha = true;
                }
            }
        }
        return view('absen.index', compact('absen', 'checkout', 'alpha'));
    }

    // Fungsi Untuk Melakukan CheckIn
    public function checkin(Request $request)
    {
        //Untuk Mengecek Hari Libur
        if (date('l') == 'Saturday' || date('l') == 'Sunday') {
            return redirect()->back()->with('error','Hari Libur Tidak bisa Check In');
        }
        // Untuk Mengecek Status Sudah CheckIn atau Belum
        $absen = Absen::whereUserId(auth()->user()->id)->whereTanggal(date('Y-m-d'))->first();
        if($absen){
            return redirect()->back()->with('error','Anda sudah Check In'); 
        }

        // Untuk Mengirim Data CheckIn
        $absen = [ 
            'tanggal' => date('Y-m-d'),
            'user_id' => auth()->user()->id
        ];
        try {
            $absen = Absen::create($absen);
        } catch (\Throwable $th) {
            return redirect()->back()->with('error','Terjadi Kesalahan saat Check In');
        }

        $data['jam_masuk']  = date('H:i:s');
        $data['lat'] = $request->lat;
        $data['lng'] = $request->lng;
        $data['absen_id'] = $absen->id;

        //Untuk Mengirim Data Checkin serta memberikan status checkin

        if (strtotime($data['jam_masuk']) > strtotime('09:15:00') && strtotime($data['jam_masuk']) <= strtotime('17:00:00')) {
            $data['keterangan'] = 'Telat';
        }else if (strtotime($data['jam_masuk']) >= strtotime('09:00:00') && strtotime($data['jam_masuk']) <= strtotime('09:15:00')) {
            $data['keterangan'] = 'Masuk';
        } else {
            $data['keterangan'] = 'Alpha';
        }
        try {
            Checkin::create($data);
        } catch (\Throwable $th) {
            return redirect()->back()->with('error','Terjadi Kesalahan saat Check Out');
        }
        alert()->success('Berhasil Checkin', 'Checkin');
        return redirect()->back();
    }

    public function checkout(Request $request)
    {
        $absen = Absen::whereUserId(auth()->user()->id)->whereTanggal(date('Y-m-d'))->first();

        // Untuk Mengecek Status Sudah CheckIn atau Belum
        $checkin = Checkin::whereAbsenId(auth()->user()->id)->first();
        if(!$absen){
            return redirect()->back()->with('error','Anda belum Check In'); 
        }

        // Untuk Mengecek Checkin Alpha atau Tidak
        if(isset($checkin)){
            if($checkin->keterangan == 'Alpha'){
                return redirect()->back()->with('error','Anda hari ini Alpha'); 
            }
        }

        //Untuk Mengirim Data Checkout serta memberikan status checkout
        $data['jam_keluar']  = date('H:i:s');
        $data['lat'] = $request->lat;
        $data['lng'] = $request->lng;
        $data['absen_id'] = $absen->id;
        if (strtotime($data['jam_keluar']) < strtotime('17:00:00')) {
            $data['keterangan'] = 'Checkout terlalu cepat';
        } else if (strtotime($data['jam_keluar']) >= strtotime('17:00:00')) {
            $data['keterangan'] = 'Checkout sesuai';
        }
        try {
            Checkout::create($data);
        } catch (\Throwable $th) {
            return redirect()->back()->with('error','Terjadi Kesalahan saat Check Out');
        }
        alert()->success('Berhasil Checkout', 'Checkout');
        return redirect()->back();
    }

    //Fungsi Untuk Menghapus Absen
    public function hapus($id)
    {
        $data = Absen::findOrFail($id);
        try {
            $data->delete();
        } catch (\Throwable $e) {
            alert()->error('Absen Gagal Dihapus', 'Absen');
            return redirect()->back();
        }
        alert()->success('Absen Berhasil Dihapus', 'Absen');
        return redirect()->back();
    }
}
