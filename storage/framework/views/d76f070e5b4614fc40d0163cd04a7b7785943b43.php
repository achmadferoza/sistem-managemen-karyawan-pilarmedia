
<?php $__env->startSection('content'); ?>

<div class="wrapper" style="max-width: 350px;">
    <div class="logo">
        <img src="https://www.freepnglogos.com/uploads/twitter-logo-png/twitter-bird-symbols-png-logo-0.png" alt="">
    </div>
    
    <?php if(date('l') == "Sunday"): ?> 
            <div class="text-center mt-4 name">
                Absen Libur
            </div>
    <?php else: ?>
        <?php if($alpha): ?>
        
        <div class="text-center mt-4 name">
            Status Anda Alpha, Silahkan Check in lagi Besok
        </div>
        <?php elseif($checkout !== null && $checkout->isNotEmpty()): ?>
        
            <div class="text-center mt-4 name">
                Silahkan Check in lagi Besok
            </div>
        <?php elseif($absen !== null): ?>
            
            <div class="text-center mt-4 name">
                Silahkan Check out
            </div>
            <form class="p-3 mt-3" method="POST" action="<?php echo e(route('absen.checkout')); ?>" role="form" id="form">
                <?php echo csrf_field(); ?>
                <input type="hidden" name="lat" id="lat">
                <input type="hidden" name="lng" id="lng">
                <button class="btn-danger mt-3" onclick="getLocation()" >Check out</button>
            </form>
        <?php else: ?>
            
            <?php if(strtotime(date('H:i:s')) >= strtotime('07:00:00') && strtotime(date('H:i:s')) <= strtotime('17:00:00')): ?>

                <div class="text-center mt-4 name">
                    Silahkan Check in
                </div>
                <form class="p-3 mt-3" method="POST" action="<?php echo e(route('absen.checkin')); ?>" role="form" id="form">
                    <?php echo csrf_field(); ?>
                    <input type="hidden" name="lat" id="lat">
                    <input type="hidden" name="lng" id="lng">
                </form>
                <button class="btn mt-3" onclick="getLocation()" >Check in</button>
        <?php else: ?>
            
            <div class="text-center mt-4 name">
                Belum Waktunya Check In
            </div>
            <?php endif; ?>
        <?php endif; ?>
    <?php endif; ?>
</div>

<?php $__env->stopSection(); ?>

<?php $__env->startSection('script'); ?>


<script>
    function getLocation() {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition, showError);
      } else { 
        x.innerHTML = "Geolocation is not supported by this browser.";
      }
    }
    
    function showPosition(position) {
        document.getElementById("lat").value = position.coords.latitude;
        document.getElementById("lng").value =  position.coords.longitude;
        document.getElementById("form").submit();
    }
    
    function showError(error) {
      switch(error.code) {
        case error.PERMISSION_DENIED:
          x.innerHTML = "User denied the request for Geolocation."
          break;
        case error.POSITION_UNAVAILABLE:
          x.innerHTML = "Location information is unavailable."
          break;
        case error.TIMEOUT:
          x.innerHTML = "The request to get user location timed out."
          break;
        case error.UNKNOWN_ERROR:
          x.innerHTML = "An unknown error occurred."
          break;
      }
    }
    </script>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master-absen', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Feroza\Documents\Project\management-karyawan\resources\views/absen/index.blade.php ENDPATH**/ ?>