
<?php $__env->startSection('content'); ?>
<div class="app-main__outer">
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <i class="pe-7s-user-female icon-gradient bg-mean-fruit">
                        </i>
                    </div>
                    <div>Users
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <?php if($errors->any()): ?>
                      <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                      <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <strong><?php echo e($error); ?></strong>
                        <button type="button" class="close" data-dismiss="alert" aria-label="Close">
                          <span aria-hidden="true">&times;</span>
                        </button>
                      </div>
                      <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
                <div class="main-card mb-3 card">
                    <div class="card-body">
                        <div class="row">
                            <div class="col-md-3" style="min-height: 330px">
                                <!-- Profile Image -->
                                <div class="card card-primary card-outline">
                                    <div class="card-body box-profile">
                                        <div class="text-center">
                                            <img class="profile-user-img img-fluid img-circle" src="<?php echo e(asset($data->foto)); ?>" alt="User profile picture" style="width: 90%">
                                        </div>
            
                                        <h3 class="profile-username text-center" style="font-size: 25px"></h3>
            
                                        <p class="text-muted text-center" style="font-size: 20px"></p>
                                    </div>
                                    <!-- /.card-body -->
                                </div>
                                <!-- /.card -->
                            </div>
                            <!-- /.col -->
            
                            <div class="col-md-9" style="min-height: 330px">
                                <div class="card">
                                    <div class="card-header p-2">
                                        <ul class="tabs-animated-shadow tabs-animated nav">
                                            <li class="nav-item"><a class="nav-link active" href="#settings" data-toggle="tab">Update Profile</a></li>
                                            <li class="nav-item"><a class="nav-link" href="#change-password" data-toggle="tab">Ubah Password</a></li>
                                        </ul>
                                    </div><!-- /.card-header -->
                                    <div class="card-body">
                                        <div class="tab-content">
                                            <div class="tab-pane active" id="settings">
                                                <form class="form-horizontal" action="<?php echo e(route('user.update')); ?>" method="post" enctype="multipart/form-data">
                                                    <?php echo csrf_field(); ?> <?php echo method_field('PATCH'); ?>
                                                    <input type="hidden" name="foto_old" value="<?php echo e($data->foto); ?>">
                                                    <input type="hidden" name="role_id" value="<?php echo e($data->role_id); ?>">
                                                    <div class="form-group row">
                                                        <label for="name" class="col-sm-2 col-form-label">Nama</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" class="form-control" id="name" name="nama" placeholder="Nama" value="<?php echo e($data->nama); ?>" required="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="username" class="col-sm-2 col-form-label">Username</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" class="form-control" id="username" name="username" placeholder="Username" value="<?php echo e($data->username); ?>" required="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="username" class="col-sm-2 col-form-label">No Handphone</label>
                                                        <div class="col-sm-10">
                                                            <input type="text" class="form-control" id="username" name="phone_number" placeholder="Nomor Handphone" value="<?php echo e($data->phone_number); ?>" required="">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="foto" class="col-sm-2 col-form-label">Foto</label>
                                                        <div class="col-sm-10">
                                                            <input type="file" class="form-control" id="foto" name="foto" placeholder="Foto Profile"  accept="image/png, image/gif, image/jpeg">
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="offset-sm-2 col-sm-10">
                                                            <button type="submit" class="btn btn-primary">Simpan</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
            
                                            <div class="tab-pane" id="change-password">
                                                <form class="form-horizontal" action="<?php echo e(route('user.ubah_password')); ?>" method="post" enctype="multipart/form-data">
                                                    <?php echo csrf_field(); ?> <?php echo method_field('PATCH'); ?>
                                                    <div class="form-group row">
                                                        <label for="password" class="col-sm-2 col-form-label">Password</label>
                                                        <div class="col-sm-10">
                                                            <input type="password" class="form-control" name="password" placeholder="Password" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="password" class="col-sm-2 col-form-label">Password Baru</label>
                                                        <div class="col-sm-10">
                                                            <input type="password" class="form-control" name="password_baru" placeholder="Password Baru" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <label for="password" class="col-sm-2 col-form-label">Konfirmasi Password</label>
                                                        <div class="col-sm-10">
                                                            <input type="password" class="form-control" name="konfirmasi_password" placeholder="Konfirmasi Password" required>
                                                        </div>
                                                    </div>
                                                    <div class="form-group row">
                                                        <div class="offset-sm-2 col-sm-10">
                                                            <button type="submit" class="btn btn-primary">Simpan</button>
                                                        </div>
                                                    </div>
                                                </form>
                                            </div>
                                            <!-- /.tab-pane -->
                                        </div>
                                        <!-- /.tab-content -->
                                    </div><!-- /.card-body -->
                                </div>
                                <!-- /.card -->
                            </div>
                            <!-- /.col -->
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>
<?php echo $__env->make('layouts.master-dashboard', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Feroza\Documents\Project\management-karyawan\resources\views/dashboard/profile.blade.php ENDPATH**/ ?>