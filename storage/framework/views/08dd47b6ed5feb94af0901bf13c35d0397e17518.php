
    <div class="scrollbar-sidebar">
        <div class="app-sidebar__inner">
            <ul class="vertical-nav-menu">
                <?php if(auth()->user()->role_id == 1): ?>
                    <li class="mt-3">
                        <a href="<?php echo e(url('/dashboard')); ?>">
                            <i class="metismenu-icon pe-7s-display1"></i>
                            Dashboard
                        </a>
                    </li>
                <?php endif; ?>
                <?php if(auth()->user()->role_id == 2): ?>
                    <li class="mt-3">
                        <a href="<?php echo e(url('/kehadiran')); ?>">
                            <i class="metismenu-icon pe-7s-portfolio"></i>
                            Kehadiran
                        </a>
                    </li>
                <?php endif; ?>
                <?php if(auth()->user()->role_id == 2): ?>
                    <li class="mt-3">
                        <a href="<?php echo e(url('/')); ?>">
                            <i class="metismenu-icon pe-7s-credit"></i>
                            Absen
                        </a>
                    </li>
                <?php endif; ?>
                <li class="mt-3">
                    <a href="<?php echo e(url('/profile')); ?>">
                        <i class="metismenu-icon pe-7s-user-female"></i>
                        Account Setting
                    </a>
                </li>
                <?php if(auth()->user()->role_id == 1): ?>
                    <li class="mt-3">
                        <a href="<?php echo e(url('/users')); ?>">
                            <i class="metismenu-icon pe-7s-id"></i>
                            Users
                        </a>
                    </li>
                <?php endif; ?>
                <li class="mt-3">
                    <a href="<?php echo e(url('/logout')); ?>">
                        <i class="metismenu-icon pe-7s-power"></i>
                        Logout
                    </a>
                </li>
            </ul>
        </div>
    </div><?php /**PATH C:\Users\Feroza\Documents\Project\management-karyawan\resources\views/layouts/sidebar-dashboard.blade.php ENDPATH**/ ?>