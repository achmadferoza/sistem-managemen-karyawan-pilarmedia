
<?php $__env->startSection('content'); ?>
<div class="app-main__outer">
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <i class="pe-7s-portfolio icon-gradient bg-mean-fruit">
                        </i>
                    </div>
                    <div>Daftar Kehadiran
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-xl-4">
                <div class="card mb-3 widget-content bg-midnight-bloom">
                    <div class="widget-content-wrapper text-white">
                        <div class="widget-content-left">
                            <div class="widget-heading">Masuk</div>
                        </div>
                        <div class="widget-content-right">
                            <div class="widget-numbers text-white"><span><?php echo e(count($checkin)); ?></span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xl-4">
                <div class="card mb-3 widget-content bg-sunny-morning">
                    <div class="widget-content-wrapper text-white">
                        <div class="widget-content-left">
                            <div class="widget-heading">Telat</div>
                        </div>
                        <div class="widget-content-right">
                            <div class="widget-numbers text-white"><span><?php echo e(count($telat)); ?></span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xl-4">
                <div class="card mb-3 widget-content bg-ripe-malin">
                    <div class="widget-content-wrapper text-white">
                        <div class="widget-content-left">
                            <div class="widget-heading">Alpha</div>
                        </div>
                        <div class="widget-content-right">
                            <div class="widget-numbers text-white"><span><?php echo e(count($alpha)); ?></span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="main-card mb-3 card">
                    <div class="card-body">
                        <div class="input-group mb-3 col-md-4 offset-md-8">
                            <input type="date" id="date" class="form-control" placeholder="Tanggal" aria-label="Recipient's username" aria-describedby="basic-addon2">
                            <div class="input-group-append">
                              <span class="input-group-text">Tanggal</span>
                            </div>
                          </div>
                        <table class="mb-0 table table-hover" id="dataTable">
                            <thead>
                            <tr>
                                <th>Tanggal</th>
                                <th>Jam Masuk</th>
                                <th>Keterangan Masuk</th>
                                <th>Jam Keluar</th>
                                <th>Keterangan Keluar</th>
                            </tr>
                            </thead>
                            <?php $__currentLoopData = $data; $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $item): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                                <tr>
                                    <td><?php echo e(date('Y-m-d', strtotime($item->tanggal))); ?></td>
                                    <td><?php echo e((isset($item->checkin[0]->jam_masuk))?$item->checkin[0]->jam_masuk:""); ?></td>
                                    <td><?php echo e((isset($item->checkin[0]->keterangan))?$item->checkin[0]->keterangan:""); ?></td>
                                    <td><?php echo e((isset($item->checkout[0]->jam_keluar))?$item->checkout[0]->jam_keluar:""); ?></td>
                                    <td><?php echo e((isset($item->checkout[0]->keterangan))?$item->checkout[0]->keterangan:""); ?></td>
                                </tr>
                            <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php $__env->stopSection(); ?>


<?php echo $__env->make('layouts.master-dashboard', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?><?php /**PATH C:\Users\Feroza\Documents\Project\management-karyawan\resources\views/dashboard/kehadiran.blade.php ENDPATH**/ ?>