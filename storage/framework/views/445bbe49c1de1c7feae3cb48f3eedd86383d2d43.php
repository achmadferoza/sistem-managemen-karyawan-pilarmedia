
    <div class="scrollbar-sidebar">
        <div class="app-sidebar__inner">
            <ul class="vertical-nav-menu">
                <li class="mt-3">
                    <a href="<?php echo e(url('/inventory')); ?>">
                        <i class="metismenu-icon pe-7s-display1"></i>
                        Inventory
                    </a>
                </li>
                <li class="mt-3">
                    <a href="<?php echo e(url('/transaksi-pembelian')); ?>">
                        <i class="metismenu-icon pe-7s-display1"></i>
                        Transaksi Pembelian
                    </a>
                </li>
                <li class="mt-3">
                    <a href="<?php echo e(url('/transaksi-penjualan')); ?>">
                        <i class="metismenu-icon pe-7s-display1"></i>
                        Transaksi Penjualan
                    </a>
                </li>
                <li class="">
                    <a href="<?php echo e(url('/logout')); ?>">
                        <i class="metismenu-icon pe-7s-angle-left-circle"></i>
                        Logout
                    </a>
                </li>
            </ul>
        </div>
    </div><?php /**PATH C:\Users\Feroza\Documents\Project\management-karyawan\resources\views/layouts/sidebar.blade.php ENDPATH**/ ?>