<!DOCTYPE html>
<html lang="en">
<head>
    <meta charset="UTF-8">
    <meta name="viewport" content="width=device-width, initial-scale=1.0">
    <meta http-equiv="X-UA-Compatible" content="ie=edge">
    <title>Daftar | Sistem Management Karyawan</title>
    <link rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/css/bootstrap.min.css">
    <link rel="stylesheet" href="https://use.fontawesome.com/releases/v5.7.2/css/all.css">
    <link rel="stylesheet" href="<?php echo e(asset('assets/css/style.css')); ?>">
    <link href="<?php echo e(asset('assets/sweetalert/sweetalert.css')); ?>" rel="stylesheet">
</head>
<body>
    <div class="wrapper" style="max-width: 350px;">
        <div class="logo">
            <img src="https://www.pilarmedia.com/wp-content/uploads/2021/11/logosolog.png" alt="" style="object-fit: contain;">
        </div>
        <div class="text-center mt-4 name">
            Daftar
        </div>
        <?php if($errors->any()): ?>
                <?php $__currentLoopData = $errors->all(); $__env->addLoop($__currentLoopData); foreach($__currentLoopData as $error): $__env->incrementLoopIndices(); $loop = $__env->getLastLoop(); ?>
                    <div class="alert alert-danger alert-dismissible fade show" role="alert">
                        <strong><?php echo e($error); ?></strong>
                    </div>
                <?php endforeach; $__env->popLoop(); $loop = $__env->getLastLoop(); ?>
                <?php endif; ?>
        <form class="p-3 mt-3" method="POST" action="<?php echo e(url('daftar/tambah')); ?>" role="form">
            <?php echo csrf_field(); ?>
            <div class="form-field d-flex align-items-center">
                <span class="far fa-user"></span>
                <input type="text" name="nama" id="nama" autocomplete="off" placeholder="Nama">
            </div>
            <div class="form-field d-flex align-items-center">
                <span class="far fa-user"></span>
                <input type="text" name="username" id="username" autocomplete="off" placeholder="username">
            </div>
            <div class="form-field d-flex align-items-center">
                <span>+62</span>
                <input type="text" name="phone_number" id="phone_number" autocomplete="off" placeholder="Nomor Telepon">
            </div>
            <div class="form-field d-flex align-items-center">
                <span class="fas fa-key"></span>
                <input type="password" name="password" id="password" autocomplete="off" placeholder="Password">
            </div>
            <button class="btn mt-3">Daftar</button>
        </form>
        <div class="text-center fs-6">
            <a href="<?php echo e(url('login')); ?>">Login</a>
        </div>
    </div>
    <script type="text/javascript" src="<?php echo e(asset('assets/sweetalert/sweetalert.min.js')); ?>"></script>
    
    <?php echo $__env->make('sweetalert::alert', \Illuminate\Support\Arr::except(get_defined_vars(), ['__data', '__path']))->render(); ?>
    <script rel="stylesheet" href="https://cdn.jsdelivr.net/npm/bootstrap@5.0.0-beta1/dist/js/bootstrap.bundle.min.js"> </script>
    <script rel="stylesheet" href="https://cdnjs.cloudflare.com/ajax/libs/jquery/3.2.1/jquery.min.js"> </script>
</body>
</html><?php /**PATH C:\Users\Feroza\Documents\Project\management-karyawan\resources\views/auth/daftar.blade.php ENDPATH**/ ?>