@extends('layouts.master-absen')
@section('content')

<div class="wrapper" style="max-width: 350px;">
    <div class="logo">
        <img src="https://www.freepnglogos.com/uploads/twitter-logo-png/twitter-bird-symbols-png-logo-0.png" alt="">
    </div>
    {{-- Untuk Memvalidasi Hari Libu --}}
    @if (date('l') == "Sunday") 
            <div class="text-center mt-4 name">
                Absen Libur
            </div>
    @else
        @if ($alpha)
        {{-- Untuk Memvalidasi Jika Checkin Melebihi jam 17.00 --}}
        <div class="text-center mt-4 name">
            Status Anda Alpha, Silahkan Check in lagi Besok
        </div>
        @elseif ($checkout !== null && $checkout->isNotEmpty())
        {{-- Untuk Memvalidasi Sudah Checkout --}}
            <div class="text-center mt-4 name">
                Silahkan Check in lagi Besok
            </div>
        @elseif ($absen !== null)
            {{-- Untuk Memvalidasi Sudah Checkin dan Belum Checkout--}}
            <div class="text-center mt-4 name">
                Silahkan Check out
            </div>
            <form class="p-3 mt-3" method="POST" action="{{route('absen.checkout')}}" role="form" id="form">
                @csrf
                <input type="hidden" name="lat" id="lat">
                <input type="hidden" name="lng" id="lng">
                <button class="btn-danger mt-3" onclick="getLocation()" >Check out</button>
            </form>
        @else
            {{-- Untuk Menampilkan Form Checkin jika jam 9.00 - 17.00 --}}
            @if (strtotime(date('H:i:s')) >= strtotime('07:00:00') && strtotime(date('H:i:s')) <= strtotime('17:00:00'))

                <div class="text-center mt-4 name">
                    Silahkan Check in
                </div>
                <form class="p-3 mt-3" method="POST" action="{{route('absen.checkin')}}" role="form" id="form">
                    @csrf
                    <input type="hidden" name="lat" id="lat">
                    <input type="hidden" name="lng" id="lng">
                </form>
                <button class="btn mt-3" onclick="getLocation()" >Check in</button>
        @else
            {{-- Untuk Menampilkan Jika Belum Saatnya Checkin --}}
            <div class="text-center mt-4 name">
                Belum Waktunya Check In
            </div>
            @endif
        @endif
    @endif
</div>

@endsection

@section('script')

{{-- Untuk Mengambil Lokasi User --}}
<script>
    function getLocation() {
      if (navigator.geolocation) {
        navigator.geolocation.getCurrentPosition(showPosition, showError);
      } else { 
        x.innerHTML = "Geolocation is not supported by this browser.";
      }
    }
    
    function showPosition(position) {
        document.getElementById("lat").value = position.coords.latitude;
        document.getElementById("lng").value =  position.coords.longitude;
        document.getElementById("form").submit();
    }
    
    function showError(error) {
      switch(error.code) {
        case error.PERMISSION_DENIED:
          x.innerHTML = "User denied the request for Geolocation."
          break;
        case error.POSITION_UNAVAILABLE:
          x.innerHTML = "Location information is unavailable."
          break;
        case error.TIMEOUT:
          x.innerHTML = "The request to get user location timed out."
          break;
        case error.UNKNOWN_ERROR:
          x.innerHTML = "An unknown error occurred."
          break;
      }
    }
    </script>
@endsection