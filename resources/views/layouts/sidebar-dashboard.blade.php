
    <div class="scrollbar-sidebar">
        <div class="app-sidebar__inner">
            <ul class="vertical-nav-menu">
                @if (auth()->user()->role_id == 1)
                    <li class="mt-3">
                        <a href="{{url('/dashboard')}}">
                            <i class="metismenu-icon pe-7s-display1"></i>
                            Dashboard
                        </a>
                    </li>
                @endif
                @if (auth()->user()->role_id == 2)
                    <li class="mt-3">
                        <a href="{{url('/kehadiran')}}">
                            <i class="metismenu-icon pe-7s-portfolio"></i>
                            Kehadiran
                        </a>
                    </li>
                @endif
                @if (auth()->user()->role_id == 2)
                    <li class="mt-3">
                        <a href="{{url('/')}}">
                            <i class="metismenu-icon pe-7s-credit"></i>
                            Absen
                        </a>
                    </li>
                @endif
                <li class="mt-3">
                    <a href="{{url('/profile')}}">
                        <i class="metismenu-icon pe-7s-user-female"></i>
                        Account Setting
                    </a>
                </li>
                @if (auth()->user()->role_id == 1)
                    <li class="mt-3">
                        <a href="{{url('/users')}}">
                            <i class="metismenu-icon pe-7s-id"></i>
                            Users
                        </a>
                    </li>
                @endif
                <li class="mt-3">
                    <a href="{{url('/logout')}}">
                        <i class="metismenu-icon pe-7s-power"></i>
                        Logout
                    </a>
                </li>
            </ul>
        </div>
    </div>