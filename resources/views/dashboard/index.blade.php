@extends('layouts.master-dashboard')
@section('css')
    <link rel="stylesheet" href="https://d19vzq90twjlae.cloudfront.net/leaflet-0.7/leaflet.css" />
@endsection
@section('content')
<div class="app-main__outer">
    <div class="app-main__inner">
        <div class="app-page-title">
            <div class="page-title-wrapper">
                <div class="page-title-heading">
                    <div class="page-title-icon">
                        <i class="pe-7s-display1 icon-gradient bg-mean-fruit">
                        </i>
                    </div>
                    <div>Dashboard
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-md-6 col-xl-4">
                <div class="card mb-3 widget-content bg-midnight-bloom">
                    <div class="widget-content-wrapper text-white">
                        <div class="widget-content-left">
                            <div class="widget-heading">Masuk</div>
                        </div>
                        <div class="widget-content-right">
                            <div class="widget-numbers text-white"><span>{{count($checkin)}}</span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xl-4">
                <div class="card mb-3 widget-content bg-sunny-morning">
                    <div class="widget-content-wrapper text-white">
                        <div class="widget-content-left">
                            <div class="widget-heading">Telat</div>
                        </div>
                        <div class="widget-content-right">
                            <div class="widget-numbers text-white"><span>{{count($telat)}}</span></div>
                        </div>
                    </div>
                </div>
            </div>
            <div class="col-md-6 col-xl-4">
                <div class="card mb-3 widget-content bg-ripe-malin">
                    <div class="widget-content-wrapper text-white">
                        <div class="widget-content-left">
                            <div class="widget-heading">Alpha</div>
                        </div>
                        <div class="widget-content-right">
                            <div class="widget-numbers text-white"><span>{{count($alpha)}}</span></div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-6">
                <div class="main-card mb-3 card">
                    <div class="card-header">
                        Lokasi Absen Masuk User Hari Ini
                      </div>
                    <div class="card-body">
                        <div id='map_all' style='width: 100%;height:50vh;'></div>
                    </div>
                </div>
            </div>
            <div class="col-md-12 col-lg-6">
                <div class="mb-3 card">
                    <div class="card-header-tab card-header-tab-animation card-header">
                        <div class="card-header-title">
                            <i class="header-icon lnr-apartment icon-gradient bg-love-kiss"> </i>
                            Grafik Karyawan
                        </div>
                    </div>
                    <div class="card-body">
                        <div class="tab-content">
                            <div class="tab-pane fade show active" id="tabs-eg-77">
                                <div class="card mb-3 widget-chart widget-chart2 text-left w-100">
                                    <div class="widget-chat-wrapper-outer">
                                        <div class="widget-chart-wrapper widget-chart-wrapper-lg opacity-10 m-0"><div class="chartjs-size-monitor" style="position: absolute; inset: 0px; overflow: hidden; pointer-events: none; visibility: hidden; z-index: -1;"><div class="chartjs-size-monitor-expand" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:1000000px;height:1000000px;left:0;top:0"></div></div><div class="chartjs-size-monitor-shrink" style="position:absolute;left:0;top:0;right:0;bottom:0;overflow:hidden;pointer-events:none;visibility:hidden;z-index:-1;"><div style="position:absolute;width:200%;height:200%;left:0; top:0"></div></div></div>
                                            <canvas id="myChart" width="610" height="304" style="display: block; height: 203px; width: 407px;" class="chartjs-render-monitor"></canvas>
                                        </div>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                </div>
            </div>
        </div>
        <div class="row">
            <div class="col-lg-12">
                <div class="main-card mb-3 card">
                    <div class="card-body">
                        <div class="input-group mb-3 col-md-4 offset-md-8">
                            <input type="date" id="date" class="form-control" placeholder="Tanggal" aria-label="Recipient's username" aria-describedby="basic-addon2">
                            <div class="input-group-append">
                              <span class="input-group-text">Tanggal</span>
                            </div>
                          </div>
                        <table class="mb-0 table table-hover" id="dataTable">
                            <thead>
                            <tr>
                                <th>Nama</th>
                                <th>Tanggal</th>
                                <th>Jam Masuk</th>
                                <th>Keterangan Masuk</th>
                                <th>Jam Keluar</th>
                                <th>Keterangan Keluar</th>
                                <th>Aksi</th>
                            </tr>
                            </thead>
                            @foreach ($data as $item)
                                <tr>
                                    <td>{{$item->user->nama}}</td>
                                    <td>{{$item->tanggal}}</td>
                                    <td>{{(isset($item->checkin[0]->jam_masuk))?$item->checkin[0]->jam_masuk:"";}}</td>
                                    <td>{{(isset($item->checkin[0]->keterangan))?$item->checkin[0]->keterangan:"";}}</td>
                                    <td>{{(isset($item->checkout[0]->jam_keluar))?$item->checkout[0]->jam_keluar:"";}}</td>
                                    <td>{{(isset($item->checkout[0]->keterangan))?$item->checkout[0]->keterangan:"";}}</td>
                                    <td>
                                        <button type="button"  class="btn btn-lg btn-danger text-white hapus" data-id="{{$item->id}}" data-nama="{{$item->tanggal}}" data-toggle="modal" data-target="#HapusData"><i class="fa fa-trash"></i></button>
                                        <button type="button"  data-toggle="modal" data-target="#LihatMap" class="btn btn-lg btn-success text-white LihatMap" data-lat_m="{{(isset($item->checkin[0]->lat))?$item->checkin[0]->lat:"";}}" data-lng_m="{{(isset($item->checkin[0]->lng))?$item->checkin[0]->lng:"";}}" data-lat_k="{{(isset($item->checkout[0]->lat))?$item->checkout[0]->lat:"";}}" data-lng_k="{{(isset($item->checkout[0]->lng))?$item->checkout[0]->lng:"";}}"><i class="fa fa-eye"></i></button>
                                    </td>
                                </tr>
                            @endforeach
                            <tbody>
                            </tbody>
                        </table>
                    </div>
                </div>
            </div>
        </div>
    </div>
</div>
@endsection


@section('modal')
<div class="modal fade" id="HapusData" tabindex="-1" role="dialog" aria-labelledby="HapusDataLabel" aria-hidden="true">
    <div class="modal-dialog" role="document">
      <div class="modal-content">
        <div class="modal-header">
          <h5 class="modal-title" id="HapusDataLabel">Hapus Absen <span class="nama_user"></span></h5>
          <button type="button" class="close" data-dismiss="modal" aria-label="Close">
            <span aria-hidden="true">&times;</span>
          </button>
        </div>
        <div class="modal-body">
          <form id="form-hapus" method="POST">
              @csrf @method('DELETE')
              Apakah Anda Yakin Ingin Menghapus Absen Tanggal  <span class="nama_user"></span>
        </div>
        <div class="modal-footer">
            <button type="button" class="btn btn-secondary" data-dismiss="modal">Batal</button>
            <button type="submit" class="btn btn-primary">Hapus</button>
          </form>
        </div>
      </div>
    </div>
</div>

<div class="modal fade bd-example-modal-lg" id="LihatMap" tabindex="-1" role="dialog" aria-labelledby="HapusDataLabel" aria-hidden="true">
    <div class="modal-dialog modal-lg" role="document">
      <div class="modal-content">
        <div class="modal-header">
            <button type="button" class="close" data-dismiss="modal" aria-label="Close">
              <span aria-hidden="true">&times;</span>
            </button>
          </div>
        <div class="card-body">
            <div id="container_map" style="width: 100%;height:50vh;"></div>
        </div>
      </div>
    </div>
</div>
@endsection


@section('script')
    <script>
        $('.hapus').click(function(){
            var id = $(this).data('id');
            var url = "{{url('/absen/hapus')}}" + '/' + id;
            $('#form-hapus').attr('action', url);
            $('.nama_user').html($(this).data('nama'));
        });
    </script>
    <script src="https://d19vzq90twjlae.cloudfront.net/leaflet-0.7/leaflet.js">
    <script src="https://code.jquery.com/jquery-3.2.1.slim.min.js" integrity="sha384-KJ3o2DKtIkvYIK3UENzmM7KCkRr/rE9/Qpg6aAZGJwFDMVNA/GpGFF93hXpG5KkN" crossorigin="anonymous"></script>
    <script src="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/js/bootstrap.min.js" integrity="sha384-JZR6Spejh4U02d8jOt6vLEHfe/JQGiRRSQQxSfFWpi1MquVdAyjUar5+76PVCmYl" crossorigin="anonymous"></script>
    </script>
    <script>
        $('.LihatMap').on('click', function(){
            document.getElementById('container_map').innerHTML = "<div id='map' style='width: 100%;height:50vh;'></div>"
            var map = L.map('map').setView([$(this).data('lat_m'), $(this).data('lng_m')], 6);
            if($(this).data('lat_m') == $(this).data('lat_k')){
                L.marker([$(this).data('lat_m'), $(this).data('lng_m')]).addTo(map).bindPopup("Lokasi Masuk dan Keluar Sama");
            }else{
                L.marker([$(this).data('lat_m'), $(this).data('lng_m')]).addTo(map).bindPopup("Lokasi Masuk");
                if($(this).data('lat_k')){
                    L.marker([$(this).data('lat_k'), $(this).data('lng_k')]).addTo(map).bindPopup("Lokasi Keluar");
                }
            }
            
            L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            }).addTo(map);
            
            $('#LihatMap').on('shown.bs.modal', function() {
                map.invalidateSize();
            });
        })
    
    </script>
    @if($data)
    <script>
        var planes = {!! json_encode($data) !!};
        var pin = [];
        var map = L.map('map_all').setView([planes[0].checkin[0].lat, planes[0].checkin[0].lng], 6);
            for (var i = 0; i < planes.length; i++) {
                L.marker([planes[i].checkin[0].lat, planes[i].checkin[0].lng]).addTo(map).bindPopup(planes[i].user.nama);
            }

            L.tileLayer('https://{s}.tile.openstreetmap.org/{z}/{x}/{y}.png', {
                attribution: '&copy; <a href="http://osm.org/copyright">OpenStreetMap</a> contributors'
            }).addTo(map);
    </script>
    @endif
    @if (isset($chart['total']))
        <script>
        // setup 
        var i = 0;
        const data = {
        
        labels: {!!json_encode($chart['bulan'])!!} ,
        datasets: [
            <?php $i = 0;?>
            <?php foreach($chart['total'] as $key => $item) {?>
            {
                label: {!!json_encode($chart['status'][$i])!!},
                backgroundColor: {!!json_encode($chart['warna'][$i])!!} ,
                data:  {!! json_encode(array_values($item))!!} ,
            },
            <?php $i++ ;?>
            <?php } ?>
        ]
        };

        // config 
        const config = {
        type: 'bar',
        data,
        options: {
            scales: {
            y: {
                beginAtZero: true
            }
            }
        }
        };

        // render init block
        const myChart = new Chart(
        document.getElementById('myChart'),
        config
        );
    </script>
            
    @endif
@endsection
