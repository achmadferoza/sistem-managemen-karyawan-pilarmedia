<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use DB;
use Hash;

class UserSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        DB::table('users')->insert([
            'role_id'   => '1',
            'nama'      => 'admin',
            'username'       => 'admin',
            'foto'      => '/storage/user/default.jpg',
            'password'  => Hash::make('123123123')
        ]);

        DB::table('users')->insert([
            'role_id'   => '2',
            'nama'      => 'feroza',
            'username'       => 'achmad_feroza',
            'foto'      => '/storage/user/default.jpg',
            'password'  => Hash::make('123123123')
        ]);
    }
}
